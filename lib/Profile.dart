import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
        body: ListView(
          children: [
            Container(
              height: 90,
              width: 390,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50),
                  ),
                  color: Colors.blueGrey
              ),
          child: Stack(
              children: <Widget>[
          Container(
          padding: EdgeInsets.only(left: 25.0,top: 20.0),
          child: Column(
            children: [
                  Row(
                    children: [
                              Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("ยินดีต้อนรับ",
                    style: TextStyle(fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.white)
                ),
                Text("นางสาว วรินทร์รตา ฤทธิ์เดช",
                    style: TextStyle(fontSize: 16,
                        fontWeight: FontWeight.normal,
                        color: Colors.white)
                )
              ],
          ),
          ],
    )
              ]
          )
          ),
    ],
    )
    ),
            SizedBox(height: 30),
            Container(
              padding: EdgeInsets.all(5.0),
                child: Column(
                    children: [
                      Image(image: NetworkImage("https://scontent.fbkk22-8.fna.fbcdn.net/v/t1.15752-9/327034734_893420681892128_4277893723797679601_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeGfSL9Hg1asaUEB1mPjPcjSA4VHyMEtcVEDhUfIwS1xUa8NnjuF1AWXGpNlC-7HN-ju8gXT0CD2qX-7VkePGmXm&_nc_ohc=0GP5NAiW1lEAX__u6jy&tn=HmN0_GWEI_eRIR6q&_nc_ht=scontent.fbkk22-8.fna&oh=03_AdS42lyXykvfcbpVNZuU2B4BUnUmUYhACMic8lVBue_gBA&oe=6401BF3F"),
                        width: 200,
                        height: 200,
                        fit: BoxFit.cover,
                      ),
                      ]
            ),
            ),
            SizedBox(height: 30),
            Container(
              height: 400,
              padding: EdgeInsets.only(left: 10.0),
              color: Colors.blueGrey,
              child: Column(
                children: [
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("ข้อมูลด้านการศึกษา",
                            style: TextStyle(
                              fontSize: 22,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(height: 5),
                          Text("รหัสประจำตัว :  63160219",style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                          ),
                          SizedBox(height: 5),
                          Text("คณะ :  คณะวิทยาการสารสนเทศ", style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                          ),
                          SizedBox(height: 5),
                          Text("วิทยาเขต: :  บางแสน", style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                          ),
                          SizedBox(height: 5),
                          Text("ระดับการศึกษา :  ปริญญาตรี", style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                          ),
                          SizedBox(height: 5),
                          Text("ปีการศึกษาที่เข้า :  2563 / 1", style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),

                          ),
                          SizedBox(height: 5),
                          Text("อ. ที่ปรึกษา :  อาจารย์ภูสิต กุลเกษม,", style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                          ),
                          Text("ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน", style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                          ),
                          SizedBox(height: 5),
                          Text("หน่วยกิตคำนวณ :   89", style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                          ),
                          SizedBox(height: 5),
                          Text("หน่วยกิตที่ผ่าน :   89", style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                          ),
                          SizedBox(height: 5),
                          Text("คะแนนเฉลี่ยสะสม :   2.92", style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
    ]
    )
    );

  }
}