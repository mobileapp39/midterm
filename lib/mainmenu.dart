import 'package:flutter/material.dart';
import 'package:wrap_widget/regisresults.dart';
import 'package:wrap_widget/register.dart';
import 'package:wrap_widget/table.dart';

import 'Profile.dart';

class mainmenu extends StatefulWidget {
  const mainmenu({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  State<mainmenu> createState() => _MainPage();
}

class _MainPage extends State<mainmenu> {
  int currentIndex = 0;
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        title: Text("มหาวิทยาลัยบูรพา"),
        elevation: 0,
        backgroundColor: Colors.blueGrey,

      ),
      body: Bottom(),
    );
  }
}

class Bottom extends StatefulWidget {
  const Bottom({Key? key}) : super(key: key);

  @override
  State<Bottom> createState() => _BottomBar();
}

class _BottomBar extends State<Bottom> {
  int _selectedIndex = 0;
  static final List<Widget> _widgetOptions = <Widget>[
    // HomeScreen(),
    table(),
    register(),
    results(),
    Profile(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions[_selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        items: const [
          BottomNavigationBarItem(
              icon: Icon(Icons.calendar_month),
              label: 'ตารางเรียน',
              ),
          BottomNavigationBarItem(
              icon: Icon(Icons.app_registration),
              label: "ลงทะเบียน"),
          BottomNavigationBarItem(
              icon: Icon(Icons.receipt),
              label: "ผลการลงทะเบียน"),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              label: "ประวัติส่วนตัว"),
        ],
      ),
    );
  }
}


