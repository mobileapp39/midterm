import 'package:flutter/material.dart';
import 'package:wrap_widget/mainmenu.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);
  @override
  State<LoginPage> createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                image: NetworkImage("https://scontent.fbkk22-2.fna.fbcdn.net/v/t1.15752-9/308140369_453523686888650_6866190560357103961_n.png?stp=dst-png_s2048x2048&_nc_cat=106&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeEI1XxdNvDVG4ioqqN3SwpldJb8JJoYYfF0lvwkmhhh8SOuGX_UQQTAkMoKGG7plk1h_79CPBgTFifHU4x-TA25&_nc_ohc=89tTsTykMlUAX8odglF&tn=HmN0_GWEI_eRIR6q&_nc_ht=scontent.fbkk22-2.fna&oh=03_AdQw4Z1xnxapr2cfe0T2rzx4zxjg81BUYOwFpwx_zrgXKQ&oe=6400DAF0"),
                width: 200,
                height: 200,
                fit: BoxFit.fill,
              ),
              SizedBox(height: 20),
              Text('มหาวิทยาลัยบูรพา',style: TextStyle(fontWeight: FontWeight.bold,
                  fontSize: 36,
                  color: Colors.grey),
              ),
              SizedBox(height: 40),

              Padding(padding: const EdgeInsets.symmetric(horizontal: 25.0),
                child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'รหัสนิสิต',
                        ),
                      ),
                    )
                ),
              ),
              SizedBox(height: 10),
              Padding(padding: const EdgeInsets.symmetric(horizontal: 25.0),
                child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'รหัสผ่าน',
                        ),
                      ),
                    )
                ),
              ),
              SizedBox(height: 25),

              Padding(padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Center(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.blueGrey
                      ),
                      child: Text(
                        'เข้าสู่ระบบ',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                    ),
                    onPressed: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const mainmenu()),
                      );
                    }
                    ),

                    // ),
                  ),
                ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }
}



