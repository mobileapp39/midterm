import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class results extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: content(),
    );
  }

  Widget content() {
    return Column(
      children: [
        SizedBox(height: 15.0),
        Container(
          // padding: EdgeInsets.only(left: 10.0),
          height: 635,
          width: 390,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50),
                topRight: Radius.circular(50),
              ),
              color: Colors.blueGrey
          ),
    child: Stack(
    children: <Widget>[
    Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
    Padding(padding: EdgeInsets.only(top: 30.0,left: 20.0),
    child: Text("ผลการลงทะเบียน",style: TextStyle(
    color: Colors.white,
    fontSize: 20,
    fontWeight: FontWeight.bold
    ),
    ),
    ),
    SizedBox(height: 20.0),
    Container(
    padding: EdgeInsets.only(left: 20.0),
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
    Text("รหัสวิชา 88624359",
    style: TextStyle(fontSize: 16,
    fontWeight: FontWeight.normal,
    color: Colors.white)
    ),
    SizedBox(height: 10.0),
    Text("ชื่อรายวิชา Web Programming",
    style: TextStyle(fontSize: 16,
    fontWeight: FontWeight.normal,
    color: Colors.white)
    ),
    SizedBox(height: 5.0),
    Text("หน่วยกิต 3  กลุ่ม 2",
    style: TextStyle(fontSize: 16,
    fontWeight: FontWeight.normal,
    color: Colors.white)
    ),
    SizedBox(height: 20.0),
      Text("รหัสวิชา 88624459",
          style: TextStyle(fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.white)
      ),
      SizedBox(height: 10.0),
      Text("ชื่อรายวิชา Object-Oriented Analysis and Design",
          style: TextStyle(fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.white)
      ),
      SizedBox(height: 5.0),
      Text("หน่วยกิต 3  กลุ่ม 2",
          style: TextStyle(fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.white)
      ),
      SizedBox(height: 20.0),
      Text("รหัสวิชา 88624559",
          style: TextStyle(fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.white)
      ),
      SizedBox(height: 10.0),
      Text("ชื่อรายวิชา Software Testing",
          style: TextStyle(fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.white)
      ),
      SizedBox(height: 5.0),
      Text("หน่วยกิต 3  กลุ่ม 2",
          style: TextStyle(fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.white)
      ),
      SizedBox(height: 20.0),
      Text("รหัสวิชา 88634259",
          style: TextStyle(fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.white)
      ),
      SizedBox(height: 10.0),
      Text("ชื่อรายวิชา Multimedia Programming for Multiplatforms",
          style: TextStyle(fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.white)
      ),
      SizedBox(height: 5.0),
      Text("หน่วยกิต 3  กลุ่ม 2",
          style: TextStyle(fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.white)
      ),
      SizedBox(height: 20.0),
      Text("รหัสวิชา 88634459",
          style: TextStyle(fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.white)
      ),
      SizedBox(height: 10.0),
      Text("ชื่อรายวิชา Mobile Application Development I",
          style: TextStyle(fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.white)
      ),
      SizedBox(height: 5.0),
      Text("หน่วยกิต 3  กลุ่ม 2",
          style: TextStyle(fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.white)
      ),
      SizedBox(height: 20.0),

    ],
    ),
    ),
    ],
    ),
    ],
    ),
    ),
    ],
    );
  }
}