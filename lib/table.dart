import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
class table extends StatelessWidget {
  DateTime today = DateTime.now();

  @override
  Widget build(BuildContext context) {
  return Scaffold(
  body: content(),
  );
  }

  Widget content(){
  return Column(
    children: [
      Container(
        child: TableCalendar(
          locale: "en_US",
          rowHeight: 43,
          headerStyle: HeaderStyle(formatButtonVisible: false),
          focusedDay: today,
          firstDay: DateTime.utc(2010,10,16),
          lastDay: DateTime.utc(2030,3,14),
        ),
      ),
      SizedBox(height: 10),
      Container(
        padding: EdgeInsets.only(left: 30),
        height: 345,
        width: 370,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(50),
              topRight: Radius.circular(50),
          ),
          color: Colors.blueGrey
        ),
        child: Stack(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(padding: EdgeInsets.only(top: 30),
                child: Text("วันนี้",style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold
                ),
                ),
                ),
                SizedBox(height: 10.0),
                Container(
                  padding: EdgeInsets.only(left: 5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("10.00 น. - 12.00 น.",
                        style: TextStyle(fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Colors.white)
                      ),
                      SizedBox(height: 10.0),
                      Text("88634459-59   Mobile Application Development I",
                          style: TextStyle(fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Colors.white)
                      ),
                      SizedBox(height: 5.0),
                      Text("IF-4C02",
                          style: TextStyle(fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Colors.white)
                      ),
                      SizedBox(height: 20.0),
                      Text("13.00 น. - 15.00 น.",
                          style: TextStyle(fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)
                      ),
                      SizedBox(height: 10.0),
                      Text("88634259-59   Multimedia Programming for Multiplatforms",
                          style: TextStyle(fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Colors.white)
                      ),
                      SizedBox(height: 5.0),
                      Text("IF-4C01",
                          style: TextStyle(fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Colors.white)
                      ),
                      SizedBox(height: 20.0),
                      Text("15.00 น. - 17.00 น.",
                          style: TextStyle(fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)
                      ),
                      SizedBox(height: 10.0),
                      Text("88624359-59   Web Programming",
                          style: TextStyle(fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Colors.white)
                      ),
                      SizedBox(height: 5.0),
                      Text("IF-3C01",
                          style: TextStyle(fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Colors.white)
                      ),
                    ],
                  ),
                )
              ],
            )
          ],
        ),

      )
    ],
  );
  }
}